export const Colors: {[index: string]: string } = {
    bg: "#F1F1F1",
    text: '#3E3838',
    textActive: '#F4B862',
    textGray: '#A5A3B0',
    white: '#FAFBFD',
    yellow: '#FFD984'
   };
   